@Regression
Feature:  BINS API CRUD Scenarios

  @CreateBIN
  Scenario Outline: As a user, I should be able to create a new BIN successfully
    Given I have a Create Bin request body with "<Text>", "<AccessKey>" and default headers
    When I call "createBinsAPI" with "POST" http request
    Then the response code is <Code>
    And "sample" in response body is "<Text>"
    And "id" is not null
    And Bin is not private
    Examples:
      | AccessKey                                                    | Text           | Code |
      | $2b$10$Lc8.SAyzeJUqdoYfBIx8LOtX7hNWuSnC0c320TFpKQc3wt8Uti7t. |  Hello Quinn!  | 200  |

  @GetBIN
  Scenario Outline: As a user, I want to be able to get a BIN successfully
    Given I have a Get Bin request body with "<Text>", "<AccessKey>" and default headers
    When I call "readBinsAPI" with "GET" http request
    Then the response code is <Code>
    And "sample" in response body is "<Text>"
    And response bin id is the same as newly created bin id
    And Bin is not private
    Examples:
      | AccessKey                                                    | Text           | Code |
      | $2b$10$Lc8.SAyzeJUqdoYfBIx8LOtX7hNWuSnC0c320TFpKQc3wt8Uti7t. |  Hello Quinn!  | 200  |

  @UpdateBIN
  Scenario Outline: As a user, I want to be able to update a BIN successfully
    Given I have an Update Bin request body with "<RequestText>", "<AccessKey>", "<Text>" and default headers
    When I call "updateBinsAPI" with "PUT" http request
    Then the response code is <Code>
    And "sample" in response body is "<Text>"
    And parent bin id is the same as newly updated bin id
    And Bin is not private
    Examples:
      | AccessKey                                                    | RequestText    | Code | Text                |
      | $2b$10$Lc8.SAyzeJUqdoYfBIx8LOtX7hNWuSnC0c320TFpKQc3wt8Uti7t. |  Hello Quinn!  | 200  | Hello QA Engineers! |

  @DeleteBIN
  Scenario Outline: As a user, I want to be able to delete a BIN successfully
    Given I have a Delete Bin request body with "<AccessKey>", "<Text>" and default headers
    When I call "deleteBinsAPI" with "DELETE" http request
    Then the response code is <Code>
    And response bin id is the same as newly created bin id
    And success message is: "<message>"
    Examples:
      | AccessKey                                                    | Text           | Code | message                  |
      | $2b$10$Lc8.SAyzeJUqdoYfBIx8LOtX7hNWuSnC0c320TFpKQc3wt8Uti7t. |  Hello Quinn!  | 200  | Bin deleted successfully |



