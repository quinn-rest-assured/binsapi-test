package com.quinn.bins.api.stepDefinition;

import com.quinn.bins.api.actions.APIResources;
import com.quinn.bins.api.actions.HttpActions;
import com.quinn.bins.api.actions.Utils;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.io.IOException;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class BinsApiSteps extends Utils {
    RequestSpecification res;
   HttpActions httpActions = new HttpActions();
   ResponseSpecification responseSpecification;
   Response response;
   static String binId;

    @Given("I have a Create Bin request body with {string}, {string} and default headers")
    public void iHaveACreateBinRequestBodyAndDefaultHeaders(String text, String accessKey) throws IOException {
        res = given(getRequestSpecification(accessKey))
                .body(httpActions.createBinRequest(text));
    }

    @When("I call {string} with {string} http request")
    public void iCallWithHttpRequest(String resource, String methodType) {
        APIResources apiResources = APIResources.valueOf(resource);
        System.out.println(apiResources.getResource());

        if(methodType.equalsIgnoreCase("POST")){
            response = res.when().post(apiResources.getResource());
        }
        else if(methodType.equalsIgnoreCase("GET")) {
            response = res.when().get(apiResources.getResource());
        }
        else if(methodType.equalsIgnoreCase("PUT")) {
            response = res.when().put(apiResources.getResource());
        }
        else if(methodType.equalsIgnoreCase("DELETE")) {
            response = res.when().delete(apiResources.getResource());
        }

    }

    @Then("the response code is {int}")
    public void theResponseCodeIs(int code) {
        responseSpecification = new ResponseSpecBuilder().expectStatusCode(200).build();
        assertEquals(200, response.getStatusCode());
    }

    @And("{string} in response body is {string}")
    public void inResponseBodyIs(String key, String expectedValue) {
        assertEquals(expectedValue, getJsonPath(response, "record.sample"));
        System.out.println("The request and response have the same " + key +  "  values");
    }

    @And("{string} is not null")
    public void isNotNull(String iid) {
        String newBinId = getJsonPath(response, "metadata.id");
        assertNotNull(newBinId);
        System.out.println("Generated Metadata id is " + newBinId);
    }

    @And("Bin is not private")
    public void binIsNotPrivate() {
        assertEquals("false", getJsonPath(response, "metadata.private"));
    }

    @Given("I have a Delete Bin request body with {string}, {string} and default headers")
    public void iHaveADeleteBinRequestBodyWithAndDefaultHeaders(String accessKey, String text) throws IOException {
        createANewBin(text, accessKey);
        binId=getJsonPath(response,"metadata.id");
        System.out.println("Generated Bin Id is " + binId);

        res = given(getRequestSpecification(accessKey)).pathParam("id", binId);
    }

    @And("response bin id is the same as newly created bin id")
    public void createdBinIdIsTheSameAsBinIdDeleted() {
        String responseBinId = getJsonPath(response, "metadata.id");
        assertEquals("Response Bin Id is the same as Created Bin Id", responseBinId, binId);
    }

    @And("success message is: {string}")
    public void messageIs(String message) {
        String responseMessage = getJsonPath(response, "message");
        assertEquals(message, responseMessage);
    }


    @Given("I have a Get Bin request body with {string}, {string} and default headers")
    public void iHaveAGetBinRequestBodyWithAndDefaultHeaders(String text, String accessKey) throws IOException {
        createANewBin(text, accessKey);
        binId=getJsonPath(response,"metadata.id");
        System.out.println("Generated Bin Id is " + binId);

        res = given(getRequestSpecification(accessKey)).pathParam("id", binId);
    }

    @Given("I have an Update Bin request body with {string}, {string}, {string} and default headers")
    public void iHaveAnUpdateBinRequestBodyWithAndDefaultHeaders(String requestText, String accessKey, String text) throws IOException {
        iHaveACreateBinRequestBodyAndDefaultHeaders(requestText, accessKey);
        iCallWithHttpRequest("createBinsAPI", "POST");
        binId=getJsonPath(response,"metadata.id");
        System.out.println("Generated Bin Id is " + binId);

        res = given(getRequestSpecification(accessKey))
                .body(httpActions.createBinRequest(text))
                .pathParam("id", binId);
    }

    @And("parent bin id is the same as newly updated bin id")
    public void parentBinIdIsTheSameAsNewlyUpdatedBinId() {
        String parentBinId = getJsonPath(response, "metadata.parentId");
        assertEquals("Parent Bin Id is the same as Updated Bin Id", parentBinId, binId);
    }



    public void createANewBin(String text, String accessKey) throws IOException {
        iHaveACreateBinRequestBodyAndDefaultHeaders(text, accessKey);
        iCallWithHttpRequest("createBinsAPI", "POST");
    }
}
