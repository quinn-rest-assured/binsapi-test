package com.quinn.bins.api.actions;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.Header;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.*;

import java.io.*;
import java.util.*;

public class Utils {
    public static RequestSpecification requestSpecification;
    private static Properties properties;
    private static InputStream inputStream;

    public RequestSpecification getRequestSpecification(String accessKey) throws IOException {
        if(requestSpecification==null){
            PrintStream log = new PrintStream(new FileOutputStream("log.txt"));
            requestSpecification =  new RequestSpecBuilder()
                    .setBaseUri(getConfig("baseUrl"))
                    .addHeaders(getDefaultHeaders())
                    .addHeader("X-Master-Key", accessKey)
                    .addFilter(RequestLoggingFilter.logRequestTo(log))
                    .addFilter(ResponseLoggingFilter.logResponseTo(log))
                    .build();
        }
        return requestSpecification;
    }

    public static Map<String, String> getDefaultHeaders(){
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Bin-Private", "false");

        return headers;

    }

    public static Header getHeader(String key, String value){
        return new Header(key, value);

    }

    public String getConfig(String config) throws IOException {
        properties = new Properties();
        String fileName = "global.properties";

        inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
        properties.load(inputStream);

        return properties.getProperty(config);
    }

    public static String getJsonPath(Response response, String field){
        String responseBody = response.asString();
        JsonPath jsonPath = new JsonPath(responseBody);
        return jsonPath.get(field).toString();

       // String p = jsonPath.getString("Items.Price");

    }


}
