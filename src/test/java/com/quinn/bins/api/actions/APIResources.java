package com.quinn.bins.api.actions;

public enum APIResources {
    createBinsAPI("/b"),
    deleteBinsAPI("/b/{id}"),
    readBinsAPI("/b/{id}"),
    updateBinsAPI("/b/{id}");

    private String resource;

    APIResources(String resource) {
        this.resource = resource;
    }

    public String getResource(){
        return resource;
    }
}
