package com.quinn.bins.api.actions;

import pojo.CreateBin;

public class HttpActions {
    public CreateBin createBinRequest(String text){
        CreateBin createBin = new CreateBin();
        createBin.setSample(text);

        return createBin;
    }

}
