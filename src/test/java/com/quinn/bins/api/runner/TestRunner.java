package com.quinn.bins.api.runner;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features",
                glue =  {"com.quinn.bins.api.stepDefinition"},
                plugin = "json:target/jsonReports/cucumber-report.json")
public class TestRunner {

}
