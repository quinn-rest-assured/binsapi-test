# binsapi-test

## Description
This repository contains CRUD test scenarios for the BINS api using Rest-Assured framework with Cucumber and Java.

## Getting started  
Note that this technical case was done using a Mac.

# Testing on a PC locally
### Prerequisites for local testing
- Java JDK 11
- Maven 3
- Editor - IntelliJ

- Navigate to this repository - [BINS API](https://gitlab.com/quinn-rest-assured/binsapi-test),
- Copy Clone URL - https://gitlab.com/quinn-rest-assured/binsapi-test.git 
- Launch any Terminal on your local machine.
- Run  the following command to navigate to the folder where you want to place this repo locally, and clone repository using clone url copied above:

```
cd any_folder
git clone https://gitlab.com/quinn-rest-assured/binsapi-test.git
```

### Launching Project

- Launch Editor - IntelliJ
- Click File on the menu
- Click Open
- Navigate to folder where project was cloned (i.e. project directory)
- Select project folder
- Click Open
- Ensure all dependencies are downloaded successfully by clicking Maven Tool Windows on the top right of IntelliJ IDE, click Refresh icon. Note, If there is a pop-up to import dependencies, ensure to click Enable auto-import to also automatically download all required dependencies for the project.

### Running Tests

- Using your terminal, Navigate to the folder where project is saved locally by typing this command, for example:  ``` cd Desktop\binsapi-test ```
- Run `mvn clean verify -Dcucumber.filter.tags="@Regression"`  to execute all tests with Regression tags.
- OR Run ` mvn clean verify -Dcucumber.filter.tags="@CreateBIN" ` to run a single test - CreateBIN. The same can be done for other scenarios, by replacing tag here with the corresponding tag in feature file.

### API Logs

To view logs of every request and response that was sent and received during test execution, the file _log.txt_ contains all these logs.

### View Test Reports

After executing all tests, Navigate to _reports/html-reports/cucumber-html-reports_ to view reports. For example, To view report for all features, open _overview-features.html_ on a browser. Select the test name to view detailed report of all scenarios.

***

# Testing & Running via Gitlab CI Pipeline

- Navigate to this repository, on CI/CD page, select Pipelines, click `Run Pipeline` at the top right of the page.
- Once pipeline job is completed, select the latest pipeline, select also any of the stages: _install_, _api-tests_ to view detailed logs for each stage.
- To view Summary Reports, navigate to _api-tests_ job, on the right hand side of the page, click Browse, and follow artifacts directory until you see _overview-features.html_

Note that report is only saved for 3 days now.

# About Test framework
## Test Approach
Tests were created independently of one another, so that no feature/scenario depends on any other to run.

## Framework choice
I implemented the technical case using Rest Assured framework with Cucumber written in Java.

## Tests covered
BIN API - Create, Read, Update and Delete BINS.

## Decisions I made while implementing this solution
I implemented a POJO class to deserialize and serialize an api request body by also defining getters and setters to easily access all fields within a request body. In this case, only Create and Update BIN have request body, and they are the same. Hence, only one POJO class is implemented and used for both apis.

I implemented an `enum` file called APIResources where I defined all endpoints, so that this can easily be maintained and reduces more lines of code, when I use switch or if/else statements instead.

Also, I chose to have a configuration file called, global.properties which contain the base URL, so that one can easily change this parameter, also add new ones to it, without having to manipulate the test codes. This way, the test framework can be easily maintained.

Cucumber was also added in order for non-technical users to be able to read and understand the test coverage adequately. Also, for anyone to be able to implement new test scenarios easily without having to write any code.

Logging test steps - I also printed out responses and results of every scenario to the a log file, log.txt for easier troubleshooting.

I also chose to create a reusable class, Utils which contains reusable methods, for example, building requests and saving response body easily so it that can be reused across the framework.


# Author
Zainab Igwe-Adeyemi
